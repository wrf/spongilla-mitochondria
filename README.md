![RAxML_bipartitions.rbs_spongillida_mt_color_bold.png](https://bitbucket.org/repo/XkgaAd/images/772387459-RAxML_bipartitions.rbs_spongillida_mt_color_bold.png)

## Data from paper: ##
Francis, W.R., M. Eitel, S. Vargas, S. Krebs, H. Blum, G. Woerheide (2016) Mitochondrial genomes of the freshwater sponges *Spongilla lacustris* and *Ephydatia cf. muelleri*. **Mitochondrial DNA: Part B**

DOI: 10.1080/23802359.2016.1157771

Direct link for the paper is [here](http://www.tandfonline.com/doi/full/10.1080/23802359.2016.1157771)

Section for [Downloads](https://bitbucket.org/wrf/spongilla-mitochondria/downloads) contains alignments for protein and nucleotide CDS and relevant trees.

Genbank format annotations can be viewed in [Source](https://bitbucket.org/wrf/spongilla-mitochondria/src) (click "Raw" to view or save).